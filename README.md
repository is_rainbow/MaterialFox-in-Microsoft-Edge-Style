## MaterialFox
# But In Microsoft Edge Style
*this is how the internet working*

![Preview](https://github.com/isNijikawa/MaterialFox-in-Microsoft-Edge-Style/blob/master/asset/this%20is%20how%20the%20internet%20work.png)

## What this does
Make firefox ui style turn into chrome 'en in to Edge.

I mean, why not? Edge is still chromium based!

## What version do I use?
I do modify style sheet with FireFox 91.0 (64-bit).

Thats meaning at least it work on FireFox 91.0 (64-bit). Don't know if this can work on subsequent versions.

Backward compatible? Go do it yourself.

## Installation
1. Enable "toolkit.legacyUserProfileCustomizations.stylesheets" & "layout.css.backdrop-filter.enabled" in about:config
3. Download this project.
4. Unzip the zip, and drag folder "chrome" into firefox profile dictionary.
5. Open ya browser, then enjoy😅.

## Please note
+ If this thing not work, it's not work, I won't fix issue whether you New Issue.

### Change log
> #### ff94.0
> Fix:
> + Fix popup menu buttons solid color.
>
> Known issues:
> - Urlbar text may unaligned if the Windows Display Scale modified.

> #### ff92.0 final version
> Rebase style sheet from [this][1] repo.
> 

> #### v92.0 9/15/2021 update
> Fix:
> + Fix some urlbar icon border radius issues.
> + Fix searchbar inner icon position issue.
> + Reduce searchbar container vertical position.
> + Reduce the acrylic luminosity.
> 
> Known issues:
> 
> - The urlbar icon hover highlight 1 or 2 pixie lower then the icon image, don't know where the issue cause of.
> - The window button in the upper right corner is 4 pixels lower than the border. 

> #### v92.0 9/10/2021 update
> + Since this ff version, the basic MaterialFox wont be work correctly anymore, the fully compactable EdgeMaterialFox has a release as V91. All the subsequent updates will release as a preview.

> #### v91
> + Update Microsoft Acrylic effect.
> 
> #### v90
> + Base version of this repository.
>


### TODO
+ Make acrylic effect into the setting popup menu.

#### 要饭
This theme is powered by blood, sweat, and coffee.

But not mine¡ So If you like it, please consider helping origin repository author to support him continued development.

[![Buy him a coffee](https://github.com/isNijikawa/MaterialFox-in-Microsoft-Edge-Style/blob/master/asset/icon.svg)](https://www.buymeacoffee.com/n4ho5QX2l)

##### Me? oh Come'n! I am all voluntary.

[1]: https://github.com/bmFtZQ/Edge-FrFox
